function goTo(direction) {
	if ('allp' === direction) {
		window.location.href = "/lumen/all-posts";
	} else if ('addp' === direction) {
		window.location.href = "/lumen/add-post";
	} else {
		window.location.href = "/lumen/";
	}
}
