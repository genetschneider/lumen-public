<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>


    <link href="/lumen/resources/css/custom.css" rel="stylesheet" type="text/css"/>
    <link href="/lumen/resources/css/animista.css" rel="stylesheet" type="text/css"/>

    <title>Lumen blog</title>
</head>
<body>
<div class="container mainContent">
    <!--header-->
    <div class="row">
        <div class="col-md-12 header">
            <h1 class="headerHome">Lumen</h1>
            <h5>A simple blog using <a href="https://lumen.laravel.com/">Lumen</a> microframework.</h5>
            <hr>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-md-12 text-center">
            <div class="allPostDivClick col-md-4" onclick="goTo('allp')">
                All posts
            </div>
            <div class="addPostDivClick col-md-4" onclick="goTo('addp')">
                Add post
            </div>
            <div class="infoClick col-md-4" onclick="goTo('info')">
                Info
            </div>
        </div>
    </div>

    <div class="row text-center aboutthis">
        <hr>
        <div class="col-md-12">
            <p class="lead">
                About this site
            </p>
            <p>
                This website has been developed in Lumen micro-framework by Genet Schneider as Thesis work.
            </p>
        </div>
    </div>
</div>
</body>
<script src="/lumen/resources/js/app.js"></script>
</html>