<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
		  integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<script
		src="https://code.jquery.com/jquery-3.1.1.min.js"
		integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
		crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
			integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
			crossorigin="anonymous"></script>


	<link href="/lumen/resources/css/custom.css" rel="stylesheet" type="text/css"/>
	<link href="/lumen/resources/css/animista.css" rel="stylesheet" type="text/css"/>

	<title>Lumen blog</title>
</head>
<body>
<div class="container mainContent">
	<!--header-->
	<div class="row">
		<div class="col-md-12 header">
			<h1 class="headerHome" onclick="goTo('info')">Lumen</h1>
			<h5>A simple blog using <a href="https://lumen.laravel.com/">Lumen</a> microframework.</h5>
			<hr>
		</div>
	</div>

	<div class="row text-center">
		<div class="col-md-12 text-center">
			<div class="allPostDivClick col-md-4" onclick="goTo('allp')">
				All posts
			</div>
			<div class="addPostDivClick col-md-4" onclick="goTo('addp')">
				Add post
			</div>
			<div class="infoClick col-md-4" onclick="goTo('info')">
				Info
			</div>
		</div>
	</div>

	<div class="row text-center">
		<div class="col-md-6 col-md-offset-3 addPostDiv">
			<h3>Add a post</h3>
			<form action="/lumen/insert-post" method="post">
				<div class="form-group">
					<label for="name">Name</label>
					<input autofocus type="text" class="form-control" name="name" id="name"
						   placeholder="Enter your name"
						   required>
				</div>
				<div class="form-group">
					<label for="title">Title</label>
					<input type="text" class="form-control" name="title" id="title" placeholder="Title of your post"
						   required>
				</div>
				<div class="form-group">
					<label for="content">Post</label>
					<textarea maxlength="254" class="form-control" name="content" id="content" placeholder="Write about something"
							  required rows="5"></textarea>
				</div>
				<div>
					<input type="submit" class="btn btn-primary" value="Save">
				</div>
			</form>
			<h3 class="text-info">
				<?php
					if (isset($result)) {
						echo $result;
					}
				?>
			</h3>
		</div>
	</div>
</div>
</body>
<script src="/lumen/resources/js/app.js"></script>
</html>