<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', 'AppController@home');
$app->get('/add-post', 'AppController@addPost');
$app->get('/version', function () use ($app) {
	return $app->version();
});

// posts
$app->get('/all-posts', 'PostController@getAll');
$app->get('/post/{id}', 'PostController@get');
$app->get('/post-id/{id}', 'PostController@getPostId');
$app->post('insert-post', 'PostController@insert');

// users
$app->get('/all-users', 'UserController@getAll');
$app->get('user/{id}', 'UserController@get');