<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Exceptions\MissingValueException;
use App\Models\User;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class UserController extends BaseController
{
	/**
	 * get all users as array
	 * @return array
	 * @throws MissingValueException
	 */
	public function getAll(): array
	{
		$users = User::query()->get()->all();

		if (!$users) {
			throw new MissingValueException('No users found.');
		}

		return $users;
	}

	/**
	 * get user as array by id
	 * @param int $id
	 * @return array
	 * @throws MissingValueException
	 */
	public function get(int $id): array
	{
		$user = User::query()->get()->where('id', $id)->toArray();

		if (!$user) {
			throw new MissingValueException('No user found with id: ' . $id . '.');
		}

		return $user;
	}

	/**
	 * insert user to database
	 * @param Request $request
	 * @return string
	 * @throws MissingValueException
	 */
	public function insert(Request $request): string
	{
		$name = $request->input('name');

		if (!$name) {
			throw new MissingValueException('Name is required.');
		}

		return (string)User::query()->insert(['name' => $name]);
	}
}
