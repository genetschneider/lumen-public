<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Exceptions\MissingValueException;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Laravel\Lumen\Routing\Controller as BaseController;

class PostController extends BaseController
{
	/**
	 * Returns view with all the posts
	 * @return \Illuminate\View\View
	 */
	public function getAll(): View
	{
		$posts = Post::query()
			->join('users', 'users.id', '=', 'written_by')
			->orderBy('posts.id', 'desc')
			->get();

		if (!$posts) {
			return view('allPosts', [
				'posts' => [],
				'result' => 'No results.'
			]);
		}

		return view('allPosts', [
			'posts' => $posts
		]);
	}

	/**
	 * get post as array by id
	 * @param int $id
	 * @return array
	 * @throws MissingValueException
	 */
	public function get(int $id): array
	{
		$post = Post::query()->get()->where('id', $id)->toArray();

		if (!$post) {
			throw new MissingValueException('No post found with id: ' . $id . '.');
		}

		return $post;
	}

	/**
	 * method to check if we have the user in db already
	 * @param string $name
	 * @return int
	 */
	public function checkIfUserExistsReturnId(string $name): int
	{
		$user = User::query()
			->where('name', $name)
			->first();

		if (!$user) {
			return -1;
		}

		return $user['id'];
	}

	/**
	 * @param Request $request
	 * @return View
	 * @throws MissingValueException
	 */
	public function insert(Request $request): View
	{
		$title = $request->input('title');
		$content = $request->input('content');
		$name = $request->input('name');

		if (!$title || !$content || !$name) {
			throw new MissingValueException('Name, content and username are required.');
		}

		// check if we have the user in db already
		$userId = $this->checkIfUserExistsReturnId($name);

		if ($userId == -1) {
			$userId = User::query()->insertGetId([
				'name' => htmlspecialchars($name)
			]);
		}

		$insertPost = Post::query()->insert(
			[
				'title' => htmlspecialchars($title),
				'content' => htmlspecialchars($content),
				'written_by' => $userId
			]
		);

		return view('addPost', [
			'result' => $insertPost ? 'Added post!' : 'Failed to post!'
		]);
	}
}
