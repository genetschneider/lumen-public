<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\View\View;
use Laravel\Lumen\Routing\Controller as BaseController;

class AppController extends BaseController
{
	public function render($name): View
	{
		return view('hello', ['name' => $name]);
	}

	public function home(): View
	{
		return view('home');
	}

	public function addPost(): View
	{
		return view('addPost');
	}
}
